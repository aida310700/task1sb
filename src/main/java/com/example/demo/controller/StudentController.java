package com.example.demo.controller;

import com.example.demo.model.Student;
import com.example.demo.repository.StudentRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
    public class StudentController {
        private StudentRepository studentRepository;

        public StudentController(StudentRepository studentRepository){
            this.studentRepository = studentRepository;
        }

        @GetMapping("/student")
        public ResponseEntity<?> getAllStudents(){
            return ResponseEntity.ok(studentRepository.findAll());
        }

        @GetMapping("/student/{id}")
        public ResponseEntity<List<Student>> getStudentById(@PathVariable int id){
            return ResponseEntity.ok(studentRepository.findStudentById(id));
        }

    }
