package com.example.demo.repository;

import com.example.demo.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    @Query(value = "SELECT * FROM student WHERE student_id = ?", nativeQuery = true)
    List<Student> findStudentById(int id);

    @Query(value = "SELECT * FROM student", nativeQuery = true)
    List<Student> ownFindAll();
}
