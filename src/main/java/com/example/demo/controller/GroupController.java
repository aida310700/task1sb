package com.example.demo.controller;

import com.example.demo.repository.GroupRepository;
import org.apache.catalina.Group;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class GroupController {
    private GroupRepository groupRepository;

    public GroupController (GroupRepository groupRepository){
        this.groupRepository = groupRepository;
    }

    @GetMapping("/groupss")
    public ResponseEntity<?> getAllGroups(){
        return ResponseEntity.ok(groupRepository.findAll());
    }

    @GetMapping("/groupss/{id}")
    public ResponseEntity<List<Group>> getGroupById(@PathVariable int id){
        return ResponseEntity.ok(groupRepository.findGroupById(id));
    }

}
