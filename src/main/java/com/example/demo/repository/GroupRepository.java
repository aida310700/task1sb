package com.example.demo.repository;

import org.apache.catalina.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {
    @Query(value = "SELECT * FROM groupss WHERE group_id = ?", nativeQuery = true)
    List<Group> findGroupById(int id);

    @Query(value = "SELECT * FROM groupss ORDER BY group_id", nativeQuery = true)
    List<Group> ownFindAll();
}

